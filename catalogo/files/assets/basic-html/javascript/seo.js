var bookWidth = 0;
var bookHeight = 0;
var bookMinWidth = 0;
var bookScale = 1;
var topMenuWidth = 242;

var browserName = "";
var browserPrefix = "";
var isIE = false;
var versionIE = 9;

function bindReady(handler) {

    var called = false

    function ready() {
        if (called) return
        called = true
        handler()
    }

    if (document.addEventListener) {
        document.addEventListener("DOMContentLoaded", function () {
            ready()
        }, false)
    } else if (document.attachEvent) {

        // (3.1)
        if (document.documentElement.doScroll && window == window.top) {
            function tryScroll() {
                if (called) return
                if (!document.body) return
                try {
                    document.documentElement.doScroll("left")
                    ready()
                } catch (e) {
                    setTimeout(tryScroll, 0)
                }
            }
            tryScroll()
        }

        document.attachEvent("onreadystatechange", function () {

            if (document.readyState === "complete") {
                ready()
            }
        })
    }

    if (window.addEventListener)
        window.addEventListener('load', ready, false)
    else if (window.attachEvent)
        window.attachEvent('onload', ready)
    /*  else  
    window.onload=ready
    */
}


function browserVer() {


    var browser = navigator.userAgent.toLowerCase();
    var UA = browser.match(/(opera|ie|firefox|chrome|version)[\s\/:]([\w\d\.]+)?.*?(safari|version[\s\/:]([\w\d\.]+)|$)/) || [null, 'unknown', 0];
    browserName = (UA[1] == 'version') ? UA[3] : UA[1];

    switch (browserName) {
        case 'safari':
            browserPrefix = 'webkit';
            break
        case 'firefox':
            browserPrefix = 'Moz';
            break
        case 'opera':
            browserPrefix = 'O';
            break
        case 'ie':
            browserPrefix = 'ms';
            isIE = true;
            if (document.all && !document.querySelector) {

                versionIE = 7;
            }

            if (document.all && document.querySelector && !document.getElementsByClassName) {
                versionIE = 8;
            }
            break
        case 'chrome':
            browserPrefix = 'webkit';
            break
        case 'unknown':
            browserPrefix = 'webkit';
            break
    };

}

function rotateContent(elem, angle) {

    if (angle) {
        angle = angle.replace(",", ".");

        if (angle * 1 != 0) {

            if (isIE) {
                elem.style.overflow = "visible";

                var divWidthIn = 0;

                var sizeopt = { w: elem.offsetWidth, h: elem.offsetHeight };
                var cos = Math.cos(angle);
                var sin = Math.sin(angle);


                if (versionIE != 9) {
                    elem = elem.childNodes[0];
                    var filter = 'progid:DXImageTransform.Microsoft.Matrix(sizingMethod="auto expand", M11 = ' + cos + ', M12 = ' + (-sin) + ', M21 = ' + sin + ', M22 = ' + cos + ')';
                    elem.style.filter = filter;

                    var w = elem.offsetWidth;
                    var h = elem.offsetHeight;


                    if (Math.PI / 2 <= angle && angle < Math.PI) {
                        elem.style.marginLeft = Math.round(sizeopt.w * cos) + "px";
                    } else if (Math.PI <= angle && angle < 3 * Math.PI / 2) {
                        elem.style.marginLeft = Math.round(sizeopt.w * cos) + "px";
                        elem.style.marginTop = Math.round(sizeopt.w * sin) + "px";
                    } else if (3 * Math.PI / 2 <= angle && angle < 2 * Math.PI) {
                        elem.style.marginTop = Math.round(sizeopt.w * sin) + "px";
                    }


                } else {
                    elem.style[browserPrefix + 'TransformOrigin'] = "0 0 0";
                    elem.style[browserPrefix + 'Transform'] = " rotate(" + angle * 180 / Math.PI + "deg)";
                    elem.style.left = (1 * (elem.style.left.replace("px", "")) - (elem.style.width.replace("px", "")) / 2 + Math.round(sizeopt.w * cos / 2)) + "px";
                    elem.style.top = (1 * (elem.style.top.replace("px", "")) + Math.round(sizeopt.w * sin / 2)) + "px";
                }



            } else {

                elem.style[browserPrefix + 'TransformOrigin'] = "0 0 0";
                elem.style[browserPrefix + 'Transform'] = " rotate(" + angle * 180 / Math.PI + "deg)";

            }
        }
    }

}
function setBG() {
    if (isIE && versionIE != 9) {

        var elem = document.getElementById('bodyBg');
        var computedStyle = elem.currentStyle || window.getComputedStyle(elem, null);
        if (computedStyle.backgroundImage != "none") {
            var imgBG = document.createElement('img');
            imgBG.src = computedStyle.backgroundImage.substr(5, computedStyle.backgroundImage.length - 7);
            imgBG.style.width = elem.offsetWidth + "px";
            imgBG.style.height = elem.offsetHeight + "px";
            imgBG.style.position = "absolute";
            imgBG.style.top = "0px";
            imgBG.style.left = "0px";
            elem.appendChild(imgBG);
        }
    }
}

function setSubstrate() {
    if (isIE && versionIE != 9) {
        var elem = document.getElementById('Page');
        var computedStyle = elem.currentStyle || window.getComputedStyle(elem, null);

        if (computedStyle.backgroundImage != "none") {
            var imgBG = document.createElement('img');
            imgBG.src = computedStyle.backgroundImage.substr(5, computedStyle.backgroundImage.length - 7);
            imgBG.style.width = elem.offsetWidth + "px";
            imgBG.style.height = elem.offsetHeight + "px";
            imgBG.style.position = "absolute";
            imgBG.style.top = "0px";
            imgBG.style.left = "0px";
            elem.insertBefore(imgBG, elem.childNodes[0]);
        }
    }
}

function checkTocInit() {
    browserVer();
    paramSet();

    var windowWidth = getWidth();

    if (bookWidth < bookMinWidth) {
        bookScale = bookMinWidth / bookWidth;
        bookWidth = bookMinWidth;

    } else if (bookWidth > windowWidth - 300) {
        if (bookMinWidth > windowWidth - 300) {
            bookScale = bookMinWidth / bookWidth;
            bookWidth = bookMinWidth;
        } else {
            bookScale = (windowWidth - 300) / bookWidth;
            bookWidth = windowWidth - 300;
        }
    }
    checkTocSize();
    setBG();


}


function checkLongString() {
    var item = document.getElementsByTagName('td')
    for (var i = 0; i < item.length; i++) {
        var cell = item[i].parentNode.parentNode.parentNode;
        cell = (cell.tagName == "LI") ? cell : (cell.parentNode.tagName == "LI") ? cell.parentNode : NaN;

        if (item[i].offsetWidth > cell.offsetWidth - 40) {
            var cellWidth = cell.offsetWidth - 40;
            var str = item[i].getElementsByTagName('a');
            if (str[0].innerText) { str[0].innerText = str[0].innerText.substring(0, cellWidth / 8) + '...'; }
            if (str[0].innerHTML) { str[0].innerHTML = str[0].innerHTML.substring(0, cellWidth / 8) + '...'; }
        }

    }
}

function getWidth() {
    if (isIE && versionIE != 9) {
        var windowWidth = document.documentElement.clientWidth;
    } else {
        var windowWidth = document.getElementsByTagName('body')[0].offsetWidth;
    }

    return windowWidth;
}


function checkTocSize() {

    var ww = document.getElementById("divPage").offsetWidth;

    if (ww != bookWidth) {
        document.getElementById("PageHeader").style.width = bookWidth + 'px';
        document.getElementById("PageMenu").style.width = bookWidth + 'px';
        document.getElementById("divPage").style.width = bookWidth + 'px';

        if (document.getElementById("PageCopyright")) {
            document.getElementById("PageCopyright").style.width = bookWidth + 'px';
        }
    }

    checkLongString();

    var th = document.getElementById("TocList").offsetHeight;
    var hh = document.getElementById("TocHeader").offsetHeight;

    var pageHeight = document.getElementsByTagName('body')[0].offsetHeight;




    if (hh + th + 30 > pageHeight) {
        document.getElementById("divPage").style.height = hh + th + 30 + 'px';
    } else if ((bookHeight > pageHeight)) {
        if (hh + th + 30 > pageHeight - 100) {
            document.getElementById("divPage").style.height = hh + th + 30 + 'px';
        } else {
            document.getElementById("divPage").style.height = pageHeight - 100 + 'px';
        }
    }


}

function checkPageInit() {
    browserVer();
    paramSet();



    var windowWidth = getWidth();

    bookScale = 1.2;

    if (bookWidth < bookMinWidth) {
        bookScale = bookMinWidth / bookWidth;
        bookWidth = bookMinWidth;
        bookHeight = bookScale * bookHeight;
        zoom(bookScale);


    } else if (bookWidth > windowWidth - 300) {
        if (bookMinWidth > windowWidth - 300) {
            bookScale = bookMinWidth / bookWidth;
            bookWidth = bookMinWidth;
            bookHeight = bookScale * bookHeight;
            zoom(bookScale);

        } else {
            bookScale = (windowWidth - 300) / bookWidth;
            bookWidth = windowWidth - 300;
            bookHeight = bookScale * bookHeight;
            zoom(bookScale);

        }
    } else {
        bookWidth = bookScale * bookWidth;
        bookHeight = bookScale * bookHeight;
        zoom(bookScale);

    }

    checkPageSize();
    setBG();
    setSubstrate();

}


function checkPageSize() {
    var ww = document.getElementById("divPage").offsetWidth;

    if (ww != bookWidth) {
        document.getElementById("PageHeader").style.width = bookWidth + 'px';
        document.getElementById("PageMenu").style.width = bookWidth + 'px';
        document.getElementById("divPage").style.width = bookWidth + 'px';

        if (document.getElementById("PageCopyright")) {
            document.getElementById("PageCopyright").style.width = bookWidth + 'px';
        }
    }


    var hh = document.getElementById("divPage").offsetHeight;

    if (hh != bookHeight) {
        document.getElementById("divPage").style.height = bookHeight + 'px';
    }

}

function paramSet() {
    bookWidth = document.getElementById("divPage").offsetWidth;
    var vW = document.getElementById("boxVersion").offsetWidth;
    var mW = document.getElementById("boxMenu").offsetWidth;
    bookMinWidth = (vW + mW + 40 < 500) ? 500 : vW + mW + 40;

    bookHeight = document.getElementById("divPage").offsetHeight;
    bookScale = 1;
}

function zoom(step) {
    var item = document.getElementById('Page');
    var elems = item.childNodes;


    for (var i = 0; i < elems.length; i++) {
        if (elems[i].tagName == "DIV") {

            if (step != 1) {
                if (!elems[i].id) {

                    elems[i].style.top = (1 * (elems[i].style.top.replace("px", "")) * step) + "px";
                    elems[i].style.left = (1 * (elems[i].style.left.replace("px", "")) * step) + "px";

                    elems[i].style.width = (Math.abs(elems[i].style.width.replace("px", "")) * step) + "px";
                    elems[i].style.height = (Math.abs(elems[i].style.height.replace("px", "")) * step) + "px";

                    var img = elems[i].getElementsByTagName('img')[0];
                    if (img) {
                        var computedStyle = img.currentStyle || window.getComputedStyle(elems[i], null);
                        img.style.top = (1 * img.style.width.replace("px", "") * step) + "px";
                        img.style.left = (1 * img.style.width.replace("px", "") * step) + "px";

                        img.style.width = (1 * img.style.width.replace("px", "") * step) + "px";
                        img.style.height = (1 * img.style.height.replace("px", "") * step) + "px";

                    }
                }

            }
            var font = elems[i].getElementsByTagName('span')[0];
            if (font) {
                var computedStyle = font.currentStyle || window.getComputedStyle(font, null);
                if (font.style.fontSize) {
                    scale = (Math.abs(font.style.fontSize.replace("px", "")) * step);
                } else {
                    scale = (Math.abs(computedStyle.fontSize.replace("px", "")) * step);
                }

                font.style.fontSize = scale + "px";

                while ((font.offsetWidth > elems[i].offsetWidth) && (scale > 1)) {
                    scale = scale - 1;
                    font.style.fontSize = scale + "px";
                }

                var diff = ((elems[i].offsetHeight - font.offsetHeight) / 2);
                diff = diff.toFixed();
                if (diff > 0) { font.style.padding = diff + "px 0px"; }
                font.style.top = ((elems[i].offsetHeight - font.offsetHeight) / 2) + "px";
            }
            rotateContent(elems[i], elems[i].getAttribute('angle'));
        }

    }


}

function checkPublCom() {
    if (document.domain.indexOf('publ.com') != -1) {
        var parts = document.URL.split('/');
        var prevpart = '';
        for (var i = 0; i < parts.length; i++) {
            if (prevpart.toLowerCase() == 'bookdata')
                return replaceBackLink(parts[i]);
            if (parts[i].toLowerCase() == 'seo' || parts[i].toLowerCase() == 'basic-html')
                return replaceBackLink(prevpart);
            prevpart = parts[i];
        }
    }
}

function replaceBackLink(id) {
    var link = document.getElementById('fullVersionLink');
    if (link) {
        var hash = link.href.indexOf('#');
        if (hash != -1)
            link.href = 'http://' + document.domain + '/' + id + link.href.substr(hash);
        else
            link.href = 'http://' + document.domain + '/' + id;
    }
}
