<?php if ($curLang == 'en') { ?>
<!-- versão em inglês --> 

    <h2 class="title">Família FDG</h2>
    <p class="title">Telephonic Wire</p>
    <div class="anatel">
        <p>0472-05-2520</p>
    </div>
    <img src="<?php echo $mediaPath; ?>familia-fdg.png" />
    <h3 class="produtos"><span>Product Particularity</span></h3>
    <p>
        <strong>Fabrication:</strong> 
        Tinned electrolytic copper conductors, insulated with polyvinyl chloride <br /> in color, 
        twisted in up to 6 conductors.
        <br />
        <strong>Put into practice:</strong> 
        Used to distribute telephonic commutation equipment and terminal distribution.
        <br />
        <strong>Rule:</strong> 
        SDT 235-310-705.
        <br />
        <strong>NBR:</strong> 
        9123.
        <br />
        <strong>Anatel Code:</strong> 
        0472-05-2520.
    </p>
	<p><a target="_blank" href="https://sistemas.anatel.gov.br/mosaico/sch/publicView/listarProdutosHomologados.xhtml">Click here</a>
	to view the certificate of ANATEL.</p>
    <h3 class="produtos"><span>Packing</span></h3>
    <img src="<?php echo $mediaPath; ?>cabo-fdg-embalado.png" width="220" />
    <br />
    <h3 class="produtos"><span>Product Structure</span></h3>
    <img src="<?php echo $mediaPath; ?>familia-fdg-estrutura-en.png" />
    <h3 class="produtos"><span>Technical Specification</span></h3>
    <p>
        <a target="_blank" href="http://www.coopersalto.com.br/english/pdf/tecnicas/fdg.pdf">Click here</a> 
        to visualize the dimensional data and electrical features in PDF.
    </p>
    <h3 class="produtos"><span>Wrapped Wire Specification</span></h3>
    <p>
        <a target="_blank" href="http://www.coopersalto.com.br/english/pdf/coroas/coroas.pdf">Click here</a> 
        to visualize the specifications in PDF.
    </p>
    <br />
    <br />
    <p class="adobe">
        To visualize PDF files, you need to have Adobe Reader installed on your computer.
        <a target="_blank" href="http://get.adobe.com/br/reader/">Click here</a> 
        to get the latest version directly from the software manufacturer's website.
    </p>

<?php } else { ?>
<!-- versão em português -->

    <h2 class="title">Família FDG</h2>
    <p class="title">Fio Telefônico</p>
    <div class="anatel">
        <p>0472-05-2520</p>
    </div>
    <img src="<?php echo $mediaPath; ?>familia-fdg.png" />
    <h3 class="produtos"><span>Detalhes do Produto</span></h3>
    <p>
        <strong>Construção:</strong> 
        Condutores de cobre eletrolítico, estanhados, isolados com PVC em cores e torcidos em até 6 condutores.
        <br />
        <strong>Aplicação:</strong> 
        Indicado para uso em distribuição de equipamentos telefônicos de comutação e interligação de blocos 
        terminais em armários de distribuição.
        <br />
        <strong>Norma Aplicável:</strong> 
        SDT 235-310-705.
        <br />
        <strong>NBR:</strong> 
        9123.
        <br />
        <strong>Código Anatel:</strong> 
        0472-05-2520.
    </p>
	<p><a target="_blank" href="https://sistemas.anatel.gov.br/mosaico/sch/publicView/listarProdutosHomologados.xhtml">Clique aqui</a>
	para visualizar o certificado da ANATEL.</p>
    <h3 class="produtos"><span>Embalagem</span></h3>
    <img src="<?php echo $mediaPath; ?>cabo-fdg-embalado.png" width="220" />
    <br />
    <h3 class="produtos"><span>Estrutura do Produto</span></h3>
    <img src="<?php echo $mediaPath; ?>familia-fdg-estrutura.png" />
    <h3 class="produtos"><span>Especificações Técnicas</span></h3>
    <p>
        <a target="_blank" href="http://coopersalto.com.br/pdf/tecnicas/fdg.pdf">Clique aqui</a> 
        para visualizar os dados dimensionais e as características elétricas em PDF.
    </p>
    <h3 class="produtos"><span>Especificações de Coroas</span></h3>
    <p>
        <a target="_blank" href="http://coopersalto.com.br/pdf/coroas/coroas.pdf">Clique aqui</a> para 
        visualizar as especificações de coroas em PDF.
    </p>
    <br />
    <br />
    <p class="adobe">
        Para visualizar os arquivos em PDF, você precisará ter o Adobe Reader instalado no seu computador. 
        <a target="_blank" href="http://get.adobe.com/br/reader/">Clique aqui</a> 
        para obter a versão mais recente diretamente do site do fabricante do software.
    </p>
    
<?php } ?>