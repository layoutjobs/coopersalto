<?php if ($curLang == 'en') { ?>
<!-- versão em inglês --> 

    <h2 class="title">Família CTI-PE</h2>
    <p class="title">Telephonic Cable until 400 pairs</p>
    <div class="anatel">
        <p>0932-05-2520</p>
    </div>
	<div style="margin-right: -20px; text-align: right;">
		<img src="<?php echo $mediaPath; ?>familia-cti-pe.jpg" />
	</div>
    <h3 class="produtos"><span>Product Particularity</span></h3>
    <p>
        <strong>Fabrication:</strong> 
        Tinned electrolytic copper, insulated with Polyethylene.
        <br />
        <strong>Put into practice:</strong> 
        Indicated to indoor installation in industries, buildings and telephone exchange.
        <br />
        <strong>Rule:</strong> 
        SDT 235-310-702.
        <br />
        <strong>NBR:</strong> 
        10501.
        <br />
        <strong>Anatel Code:</strong> 
        0932-05-2520.
    </p>
	<p><a target="_blank" href="https://sistemas.anatel.gov.br/mosaico/sch/publicView/listarProdutosHomologados.xhtml">Click here</a>
	to view the certificate of ANATEL.</p>
    <h3 class="produtos"><span>Packing</span></h3>
    <img src="<?php echo $mediaPath; ?>cabo-cti-pe-embalado.png" width="280" />
    <br />
    <h3 class="produtos"><span>Product Structure</span></h3>
    <img src="<?php echo $mediaPath; ?>familia-cti-pe-estrutura-en.png" />
    <h3 class="produtos"><span>Technical Specification</span></h3>
    <p>
        <a target="_blank" href="http://www.coopersalto.com.br/english/pdf/tecnicas/cti-pe.pdf">Click here</a> 
        to visualize the dimensional data and electrical features in PDF.
    </p>
    <h3 class="produtos"><span>Wrapped Wire Specification</span></h3>
    <p>
        <a target="_blank" href="http://www.coopersalto.com.br/english/pdf/coroas/coroas.pdf">Click here</a> 
        to visualize the specifications in PDF.
    </p>
    <br />
    <br />
    <p class="adobe">
        To visualize PDF files, you need to have Adobe Reader installed on your computer.
        <a target="_blank" href="http://get.adobe.com/br/reader/">Click here</a> 
        to get the latest version directly from the software manufacturer's website.
    </p>

<?php } else { ?>
<!-- versão em português -->

    <h2 class="title">Família CTI-PE</h2>
    <p class="title">Cabo Telefônico até 400 pares</p>
    <div class="anatel">
        <p>0932-05-2520</p>
    </div>
	<div style="margin-right: -20px; text-align: right;">
		<img src="<?php echo $mediaPath; ?>familia-cti-pe.jpg" />
	</div>
    <h3 class="produtos"><span>Detalhes do Produto</span></h3>
    <p>
        <strong>Construção:</strong> 
        Condutores de cobre eletrolítico, estanhados, isolados com veias em polietileno.
        <br />
        <strong>Aplicação:</strong> 
        Indicado para instalações internas em indústrias, edifícios e centrais telefônicas.
        <br />
        <strong>Norma Aplicável:</strong> 
        SDT 235-310-702.
        <br />
        <strong>NBR:</strong> 
        10501.
        <br />
        <strong>Código Anatel:</strong> 
        0932-05-2520.
    </p>
	<p><a target="_blank" href="https://sistemas.anatel.gov.br/mosaico/sch/publicView/listarProdutosHomologados.xhtml">Clique aqui</a>
	para visualizar o certificado da ANATEL.</p>
    <h3 class="produtos"><span>Embalagem</span></h3>
    <img src="<?php echo $mediaPath; ?>cabo-cti-pe-embalado.png" width="280" />
    <br />
    <h3 class="produtos"><span>Estrutura do Produto</span></h3>
    <img src="<?php echo $mediaPath; ?>familia-cti-pe-estrutura.png" />
    <h3 class="produtos"><span>Especificações Técnicas</span></h3>
    <p>
        <a target="_blank" href="http://coopersalto.com.br/pdf/tecnicas/cti-pe.pdf">Clique aqui</a> 
        para visualizar os dados dimensionais e as características elétricas em PDF.
    </p>
    <h3 class="produtos"><span>Especificações de Coroas</span></h3>
    <p>
        <a target="_blank" href="http://coopersalto.com.br/pdf/coroas/coroas.pdf">Clique aqui</a> para 
        visualizar as especificações de coroas em PDF.
    </p>
    <h3 class="produtos"><span>Armazenamento</span></h3>
    <p>
        As bobinas, quando armazenadas a céu aberto, devem ficar elevadas no mínimo a 10 cm do solo, 
        para evitar o contato direto com o piso e a absorção excessiva de umidade, comprometendo a 
        integridade da bobina. <strong>Nota:</strong> Quando as bobinas forem armazenadas sob cobertura 
        e o solo for revestido e drenado, as bobinas podem ficar em contato direto com o piso.
    </p>
    <p>
        No local do armazenamento deve haver drenagem apropriada para evitar acúmulo de água próximo 
        as bobinas. As bobinas, quando fechadas, podem ser armazenadas e alinhadas umas sobre as outras, 
        de modo que não comprometa a integridade do produto acondicionado e a estrutura do carretel.
    </p>
    <p>
        As bobinas devem ser calçadas para se evitar deslocamento lateral por gravidade. Os discos 
        laterais das bobinas devem estar livres de contato com outra bobina (distância mínima de 15 cm), 
        ou com outros objetos e edificações que impeçam sua boa ventilação.
    </p>
    <p>
        Não é aconselhável o armazenamento de bobinas sobre o piso com declive acentuado, com inclinação 
        superior a 2º em relação do plano.
    </p>
    <br />
    <br />
    <p class="adobe">
        Para visualizar os arquivos em PDF, você precisará ter o Adobe Reader instalado no seu computador. 
        <a target="_blank" href="http://get.adobe.com/br/reader/">Clique aqui</a> 
        para obter a versão mais recente diretamente do site do fabricante do software.
    </p>

<?php } ?>