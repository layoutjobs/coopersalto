<?php if ($curLang == 'en') { ?>
<!-- versão em inglês --> 

    <h2 class="title">Família CCE-APL-ASF</h2>
    <p class="title">Telephonic Cable 2 to 30 pairs</p>
    <div class="anatel">
        <p>1149-06-2520</p>
    </div>
	<div style="margin-right: -20px; text-align: right;">
		<img src="<?php echo $mediaPath; ?>familia-cce-apl-asf.jpg" />
	</div>
    <h3 class="produtos"><span>Product Particularity</span></h3>
    <p>
        <strong>Fabrication:</strong> 
        Electrolytic copper conductors, insulated with Polyethylene jacket, protected in group by 
        APL cover jacket. In all perimeter long, synthetic threads are incorporated to provide 
        sustentation and high resistance to axial high-tension..
        <br />
        <strong>Put into practice:</strong> 
        Indicated to aerial electrical grid.
        <br />
        <strong>Rule:</strong> 
        SDT 235-320-705.
        <br />
        <strong>NBR:</strong> 
        10502.
        <br />
        <strong>Anatel Code:</strong> 
        1149-06-2520.
    </p>
	<p><a target="_blank" href="https://sistemas.anatel.gov.br/mosaico/sch/publicView/listarProdutosHomologados.xhtml">Click here</a>
	to view the certificate of ANATEL.</p>
    <h3 class="produtos"><span>Product Structure</span></h3>
    <img src="<?php echo $mediaPath; ?>familia-cce-apl-asf-estrutura-en.png" />
    <h3 class="produtos"><span>Technical Specification</span></h3>
    <p>
        <a target="_blank" href="http://www.coopersalto.com.br/english/pdf/tecnicas/cce-apl-asf.pdf">Click here</a> 
        to visualize the dimensional data and electrical features in PDF.
    </p>
    <h3 class="produtos"><span>Wrapped Wire Specification</span></h3>
    <p>
        <a target="_blank" href="http://www.coopersalto.com.br/english/pdf/coroas/coroas.pdf">Click here</a> 
        to visualize the specifications in PDF.
    </p>
    <br />
    <br />
    <p class="adobe">
        To visualize PDF files, you need to have Adobe Reader installed on your computer.
        <a target="_blank" href="http://get.adobe.com/br/reader/">Click here</a> 
        to get the latest version directly from the software manufacturer's website.
    </p>

<?php } else { ?>
<!-- versão em português -->

    <h2 class="title">Família CCE-APL-ASF</h2>
    <p class="title">Cabo Telefônico de 2 a 30 pares</p>
    <div class="anatel">
        <p>1149-06-2520</p>
    </div>
	<div style="margin-right: -20px; text-align: right;">
		<img src="<?php echo $mediaPath; ?>familia-cce-apl-asf.jpg" />
	</div>
    <h3 class="produtos"><span>Detalhes do Produto</span></h3>
    <p>
        <strong>Construção:</strong> 
        Condutores de cobre eletrolítico, isolados com polietileno, agrupados e protegidos por uma 
        capa APL. Ao longo do perímetro da capa externa são incorporadas fibras sintéticas, que 
        proporcionam sustentação e alta resistência às tensões axiais.
        <br />
        <strong>Aplicação:</strong> 
        Indicado para instalações de redes aéreas.
        <br />
        <strong>Norma Aplicável:</strong> 
        SDT 235-320-705.
        <br />
        <strong>NBR:</strong> 
        10502.
        <br />
        <strong>Código Anatel:</strong> 
        1149-06-2520.
    </p>
	<p><a target="_blank" href="https://sistemas.anatel.gov.br/mosaico/sch/publicView/listarProdutosHomologados.xhtml">Clique aqui</a>
	para visualizar o certificado da ANATEL.</p>
    <h3 class="produtos"><span>Estrutura do Produto</span></h3>
    <img src="<?php echo $mediaPath; ?>familia-cce-apl-asf-estrutura.png" />
    <h3 class="produtos"><span>Especificações Técnicas</span></h3>
    <p>
        <a target="_blank" href="http://coopersalto.com.br/pdf/tecnicas/cce-apl-asf.pdf">Clique aqui</a> 
        para visualizar os dados dimensionais e as características elétricas em PDF.
    </p>
    <h3 class="produtos"><span>Especificações de Coroas</span></h3>
    <p>
        <a target="_blank" href="http://coopersalto.com.br/pdf/coroas/coroas.pdf">Clique aqui</a> para 
        visualizar as especificações de coroas em PDF.
    </p>
    <h3 class="produtos"><span>Armazenamento</span></h3>
    <p>
        As bobinas, quando armazenadas a céu aberto, devem ficar elevadas no mínimo a 10 cm do solo, 
        para evitar o contato direto com o piso e a absorção excessiva de umidade, comprometendo a 
        integridade da bobina. <strong>Nota:</strong> Quando as bobinas forem armazenadas sob cobertura 
        e o solo for revestido e drenado, as bobinas podem ficar em contato direto com o piso.
    </p>
    <p>
        No local do armazenamento deve haver drenagem apropriada para evitar acúmulo de água próximo 
        as bobinas. As bobinas, quando fechadas, podem ser armazenadas e alinhadas umas sobre as outras, 
        de modo que não comprometa a integridade do produto acondicionado e a estrutura do carretel.
    </p>
    <p>
        As bobinas devem ser calçadas para se evitar deslocamento lateral por gravidade. Os discos 
        laterais das bobinas devem estar livres de contato com outra bobina (distância mínima de 15 cm), 
        ou com outros objetos e edificações que impeçam sua boa ventilação.
    </p>
    <p>
        Não é aconselhável o armazenamento de bobinas sobre o piso com declive acentuado, com inclinação 
        superior a 2º em relação do plano.
    </p>
    <br />
    <br />
    <p class="adobe">
        Para visualizar os arquivos em PDF, você precisará ter o Adobe Reader instalado no seu computador. 
        <a target="_blank" href="http://get.adobe.com/br/reader/">Clique aqui</a> 
        para obter a versão mais recente diretamente do site do fabricante do software.
    </p>

<?php } ?>