
<!-- versão em português --> 

    <h2 class="title2">Aumente a satisfação do seu cliente com pontualidade nas entregas </h2><br><br>

    <img src="<?php echo $mediaPath; ?>/satisfacao-cliente-relogio.jpg" /><br><br>
        <p>Um sistema de logística perfeito é sinônimo de satisfação perante ao cliente. Sabendo da importância da pontualidade na entrega, a Coopersalto preza para que cada remessa seja única, garantindo a satisfação total do cliente.</p>
        <p>A falta de pontualidade é hoje um dos grandes problemas em todos os setores empresariais, o que leva a frustação e até mesmo a perda do cliente. É dever da Coopersalto assegurar a qualidade do setor de logística e ter como objetivo principal a pontualidade. O planejamento para que cada entrega seja única deve incluir todas etapas, como: tempo para carregamento do veículo, paradas para abastecimento, almoço, depósito e outros eventos recorrentes. </p>
        <p>Os imprevistos também devem ser levados em consideração, para isso, a equipe da Coopersalto foca também na realização de um plano de ação que possa reverter qualquer problema no menor tempo possível. Os planos de ações devem também conter ainda informações que devem ser tomadas, além dos responsáveis por cada atividade e os novos prazos. </p>
        <p>É seguindo a importância de todas as etapas e com um plano de ação responsável que a Coopersalto garante a pontualidade em suas entregas, prezando sempre a alta qualidade e a satisfação total do cliente. </p>
    
        <p>Crédito de imagem:  <a href="https://www.flickr.com/photos/guysie/" target="_blank">GuySie/CC</a></p>
   