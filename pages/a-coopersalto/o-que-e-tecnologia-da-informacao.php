
<!-- versão em português --> 

    <h2 class="title2">O que é Tecnologia da Informação (TI)?</h2><br><br>

        <h5>Introdução</h5>
        <p>No início, os computadores eram tidos apenas como "máquinas gigantes" que tornavam possível a automatização de determinadas tarefas em instituições de ensino/pesquisa, grandes empresas e nos meios governamentais. Com o avanço tecnológico, tais máquinas começaram a perder espaço para equipamentos cada vez menores, mais poderosos e mais confiáveis. Como se não bastasse, a evolução das telecomunicações permitiu que, aos poucos, os computadores passassem a se comunicar, mesmo estando em lugares muito distantes geograficamente.</p>
        <p>Mas perceba que, desde as máquinas mais remotas e modestas até os computadores mais recentes e avançados, o trabalho com a informação sempre foi o centro de tudo. É por isso que a expressão Tecnologia da Informação (TI) é tão popular. Mas o que vem a ser isso?</p>
       <h5>Antes de tudo, a informação</h5>
       <p>A informação é um patrimônio, é algo que possui valor. Quando digital, não se trata apenas de um monte de bytes aglomerados, mas sim de um conjunto de dados classificados e organizados de forma que uma pessoa, uma instituição de ensino, uma empresa ou qualquer outra entidade possa utilizar em prol de algum objetivo.</p>
       <p>Neste sentido, a informação é tão importante que pode inclusive determinar a sobrevivência ou a descontinuidade das atividades de um negócio, por exemplo. E não é difícil entender o porquê. Basta pensar no que aconteceria se uma instituição financeira perdesse todas as informações de seus clientes ou imaginar uma pessoa ficando rica da noite para o dia por ter conseguido descobrir uma informação valiosa analisando um grande volume de dados.</p>
       <p>Diante de tamanha relevância, grandes entidades investem pesado nos recursos necessários para obter e manter as suas informações. É por isso que é extremamente raro ver empresas como bancos, redes de lojas e companhias aéreas perdendo dados essenciais ao negócio. Por outro lado, é bastante frequente o uso inadequado de informações ou, ainda, a subutilização destas. É nesse ponto que a Tecnologia da Informação pode ajudar.</p>
   