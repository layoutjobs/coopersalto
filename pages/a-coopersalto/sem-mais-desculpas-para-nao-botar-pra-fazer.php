
<!-- versão em português --> 

    <h2 class="title2">Sem mais desculpas para não botar pra fazer!</h2>
    <h2 class="title2">Confira essas dicas valiosíssimas de produtividade.</h2><br>

        <p>Você sente que nunca tem horas suficientes no seu dia para fazer tudo o que você precisa? A boa notícia é que você não é o único com questões de produtividade. Não sei você, mas eu tenho um monte de pepinos para resolver. Eu costumava ter e-mails brotando igual mato na minha caixa de entrada, uma lista de tarefas quilométrica e aquelas ideias de negócios, uma delas que eu estive pensando em começar por fora há quatro anos, implorando pela minha atenção.</p>

        <p>Ok, esse último eu ainda não consegui resolver, mas pelo menos eu melhorei na parte de controlar o monstro de duas cabeças dos e-mails e do gerenciamento de tarefas. E é assim que você também pode se tornar um ninja da produtividade.</p>
        <p>Trabalhe em etapas.</p>
        <p>Faça todos os seus calls, por exemplo, em conjunto, mesmo que estejam divididos entre projetos diferentes.</p>
        <p>Agende uma “reunião para uma pessoa”.</p>
        <p>Reserve uma sala de reuniões para utilizar sozinho ou vá a um café e concentre-se no que você precisa fazer. Esse é seu tempo reservado para focar.</p>
        <p>Organize as suas listas de tarefas pelo contexto delas.</p>
        <p>Por exemplo, suas listas podem ser divididas em:</p> 
        <p>1) Lista de calls;</p> 
        <p>2) lista de afazeres de casa;</p> 
        <p>3) lista de afazeres do trabalho; e</p> 
        <p>4) lista de e-mails para enviar.</p>

        <p>Ligue o modo chefe</p>

        <p>É tentador se sentir constantemente pressionado a “fazer”, mas, sendo da liderança, às vezes você precisa parar de fazer para pensar.</p>

        <p>Tire um tempo (eu faço isso no final do dia ou durante a manhã) para olhar para o dia ou a semana anterior, rever projetos e ações e quebrar os grandes projetos em tarefas gerenciáveis (por exemplo: se você está saindo de férias, pesquise online, verifique os preços, reserve e pague).</p>

        <p>Saiba que você sempre vai ter mais para fazer do que você consegue.</p>

        <p>Você sempre terá mais demanda do que tempo, e você sempre vai querer colocar as mãos em tudo. Mas você não consegue fazer isso — ninguém consegue, mesmo que algumas pessoas pareçam poder. Por isso, trabalhe de forma eficiente, descanse bastante e tire um tempo para curtir. Já dizia James Howell: ““Trabalho sem diversão faz de Jack um bobalhão”.</p>

        <p>Verity Noble é VP de Operações do Unreasonable Institute</p>

        <p>Artigo originalmente publicado no blog do Unreasonable Institute</p>





   