<?php if ($curLang == 'en') { ?>
<!-- versão em inglês -->  

    <h2 class="title2">Coopersalto</h2>
    <p class="title2">
        We'll let you know a little more about our history and our goals
    </p>
    <img src="<?php echo $mediaPath; ?>/a-coopersalto-1.jpg" />
    <p class="legend">Modern equipment is used in the manufacture of cables.</p>
    <p>
        On April 27, 2002, a group of 110 workers founded the Production Co-operative society of 
        Metallurgist of Salto - Coopersalto. Since then our workers started a bold project to 
        generate work and income to their families, manufacturing products keeping technology 
        and quality. Coopersalto is placed in an area of 6.000 square meters and we 
        work in fixed shifts.
    </p>
    <p>
        <strong>Technology</strong><br>
        The technology used in the production of Wires and Telephonic Cables by Coopersalto is one 
        of the most advanced in Brazil. Our output is 720 thousand kilometers a year and there are 
        two important differentials: Supplying in low quantity and a short time of delivery.
    </p>
    <img src="<?php echo $mediaPath; ?>/a-coopersalto-2.jpg" />
    <p class="legend">Factory production capacity is 720 thousand kilometers pair / year.<p>
    <p>
        <strong>Purpose of our products</strong>
    </p>
    <ul>
        <li>
            Aerial or subterranean grid in ducts;
        </li>
        <li>
            Indoor installations in companies, buildings and telephone exchange;
        </li>
        <li>
            Distribution of telephonic equipments of commutation and terminal distribution.
        </li>
    </ul>
    <img src="<?php echo $mediaPath; ?>/a-coopersalto-4.jpg" />
    <p class="legend">
        Coopersalto provides Wires and Cables in retail and figures on a quick delivery.
    </p>
    <p><img src="<?php echo $mediaPath; ?>/a-coopersalto-img_8165.jpg" /></p>
    <p><img src="<?php echo $mediaPath; ?>/a-coopersalto-img_8119.jpg" /></p>
    <p><img src="<?php echo $mediaPath; ?>/a-coopersalto-img_8121.jpg" /></p>
    <p><img src="<?php echo $mediaPath; ?>/a-coopersalto-img_8129.jpg" /></p>
    <p><img src="<?php echo $mediaPath; ?>/a-cooper-salto-img_8141.jpg" /></p>
    <p><img src="<?php echo $mediaPath; ?>/a-coopersalto-img_8151.jpg" /></p>
    <p><img src="<?php echo $mediaPath; ?>/a-coopersalto-img_8166.jpg" /></p>
    <p>
        <strong>Our View</strong><br>
        Practice good prices, fast delivery, excellent service to our customers, always seeking continuous improvement in order to comply the whole domestic market.
    </p>
    <p>
        <strong>Our Mission</strong><br>
        The Cooperative's mission is to commercialize wires and telephone cables with excellent quality in order to ensure the comfort and safety of our customers.
    </p>
    <p>
        <strong>Our Values</strong><br>
        We advocate and practice integrity and ethics.<br>
        We always seek excellence, continuous improvement and innovation.<br>
        Guaranteed Management, Policies and Processes aligned with our values.<br>
        We are the memory and the Cooperative information.<br>
        We practice social and environmental responsibility.
    </p>

    <p>
    <strong>Code of Ethics</strong><br>
    Our day-to-day in the Cooperative shall be guided by the pursuit of quality and efficiency for the results we<br>
    achieve that challenge us. The Coopersalto believes that this path must be followed with a commitment to value<br> 
    and practice-based ethics and compliance, when we relate to different audiences (internal and external) who<br>
    interact with us conduct.
    </p>
    
<?php } else { ?>
<!-- versão em português --> 

    <h2 class="title2">Parceiros</h2>
    <p class="title2">
        Conheça os Parceiros da Coopersalto
    </p>
    
    <br>
    

<?php } ?>