<?php if ($curLang == 'en') { ?>
<!-- versão em inglês -->    

    <h2 class="title2">ISO 9001:2015</h2>
    <p class="title2">
        ISO (International Organization for Standardization) is a set of rules which presents 
        requirements to quality management in manufacturing process and services
    </p>
    <p>
        Due to our advanced quality control system and the continual process of improvement 
        Coopersalto conquered the ISO 9001:2015 certification, which shows the quality of our 
        Wires and Telephonic Cables, both nationally and internationally.
    </p>
    <p>
        This certificate affirms a good management and relationship among clients and suppliers, 
        motivates the workers to keep improving and searching for total quality, creates a 
        competitive market, cost reduction and process optimization.
    </p>
    <p><strong>Click on the picture below to amplify our certificate or notification of the surveillance:</strong></p>
    <a target="_blank" href="http://coopersalto.com.br/pdf/certificado.pdf">
        <img src="<?php echo $imagesPath; ?>certificado.jpg" width="200"/>
    </a>
    <a target="_blank" href="http://coopersalto.com.br/pdf/atestado-acompanhamento.pdf">
        <img src="<?php echo $imagesPath; ?>atestado-acompanhamento.jpg" width="200"/>
    </a>

<?php } else { ?>
<!-- versão em português --> 

    <h2 class="title2">ISO 9001:2015</h2>
    <p class="title2">
        A "International Organization for Standardization", é um conjunto de normas que objetiva
        apresentar requisitos para gestão da qualidade em processos de fabricação e serviços
    </p>
    <p>
        Devido ao avançado sistema de controle de qualidade adotado pela Coopersalto, e num processo
        contínuo de melhoria do sistema de gestão da qualidade, a empresa conquistou a certificação 
        ISO 9001:2008, que representa um atestado de reconhecimento nacional e internacional à 
        qualidade do nosso trabalho na fabricação de Fios e Cabos Telefônicos.
    </p>
    <p>
        Esse certificado assegura boas práticas de gestão e relacionamento entre clientes e 
        fornecedores, possibilita maior desenvolvimento dos colaboradores, serve como alavanca na 
        busca pela qualidade total, propicia condições para maior competitividade no mercado, 
        otimização de processos e a redução de custos.
    </p>
    <p><strong>Clique na imagem abaixo para ampliar o certificado ou o atestado de acompanhamento Coopersalto:</strong></p>
    <a target="_blank" href="http://coopersalto.com.br/pdf/certificado.pdf">
        <img src="<?php echo $imagesPath; ?>certificado.jpg" width="200"/>
    </a>
    <a target="_blank" href="http://coopersalto.com.br/pdf/atestado-acompanhamento.pdf">
        <img src="<?php echo $imagesPath; ?>atestado-acompanhamento.jpg" width="200"/>
    </a>

<?php } ?>