<?php if ($curLang == 'en') { ?>
<!-- versão em inglês -->  

    <h2 class="title2">Environmental Policy</h2>
    <p class="title2">
        Our work comprehends Social, Environmental and Ethical Responsibility, searching
        for the reduction of residue and the consumption of polluting material
    </p>
    <p>
        We’re conscious about how to educate our workers and we assume a commitment of continual 
        improvement to reduce the utilization of natural resources, preserving the environment 
        and avoiding its damage, respecting and carrying out all the environmental rules.
    </p>
    <p><img src="<?php echo $mediaPath; ?>/politica-ambiental-3-img_8135.jpg" /></p>
    <img src="<?php echo $mediaPath; ?>/politica-ambiental-3-img_8162.jpg" />
    <p class="legend">
        Wires and Telephonic Cables are stored according to the preservation rules, avoiding 
        environmental damages.
    </p>
    <img src="<?php echo $mediaPath; ?>/politica-ambiental-2.jpg" />
    <p class="legend">The cooperative collaborate daily with environmental preservation through selective collection.</p>

<?php } else { ?>
<!-- versão em português --> 

    <h2 class="title2">Política Ambiental</h2>
    <p class="title2">
        Trabalhamos com Responsabilidade Social, Ambiental e Ética, através da busca<br />
        contínua da redução de resíduos e consumo de materiais poluentes
    </p>
    <p>
        Buscamos conscientizar e educar nossos colaboradores, e assumimos um compromisso com a 
        melhoria contínua para reduzir a utilização dos recursos naturais, visando à preservação 
        do meio ambiente e a prevenção dos danos ambientais, através do cumprimento da legislação 
        e demais normas ambientais vigentes.
    </p>
    <p><img src="<?php echo $mediaPath; ?>/politica-ambiental-3-img_8135.jpg" /></p>
    <img src="<?php echo $mediaPath; ?>/politica-ambiental-3-img_8162.jpg" />
    <p class="legend">
        Os Fios e Cabos Telefônicos são armazenados de acordo com as normas de prevenção vigentes, 
        sem causar danos ambientais.
    </p>
    <img src="<?php echo $mediaPath; ?>/politica-ambiental-2.jpg" />
    <p class="legend">Os cooperados colaboram diariamente com a preservação ambiental, através da coleta seletiva.</p>


<?php } ?>