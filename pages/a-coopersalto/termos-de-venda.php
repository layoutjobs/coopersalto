<?php if ($curLang == 'en') { ?>
<!-- versão em inglês -->  

    <h2 class="title2">Terms of Sale</h2>
    <p class="title2">
        Terms and General Conditions for Material Supplying
    </p>
    <p>
        <strong>Purpose</strong><br />
        Our general conditions established here are constituent of Coopersalto trading proposal 
        which we’re presenting you now. If future divergences happen, these conditions below 
        will be respected:
    </p>
    <p>
        <strong>Proposal Validity</strong><br />
        The trading proposal above mentioned will have its validity during the period established 
        on it, counting in consecutive days starting on the date of our proposal presentation, 
        there’s a possibility of reviewing it if there’s an agreement between Coopersalto 
        and the customer.
    </p>
    <p>
        <strong>Prices and Trading Conditions</strong><br />
        Prices and Trading Conditions established are the ones on our trading proposal above 
        mentioned and they will be readjusted in every invoicing according to the formulas 
        established on our trading proposal and mutual agreed.
    </p>
    <p>
        <strong>Credit Approval</strong><br />
        The supplying of the material listed on our proposal depends on the approval of credit. 
        It’s related to the information and guaranty given by the customer.
    </p>
    <p>
        <strong>Taxes</strong><br />
        Our taxes are according to the newest law of taxation. Any modification in the tax 
        base or creation of new taxes or percentages will bring up-to-date our prices so that 
        the initial economical balance is kept.
    </p>
    <p>
        <strong>Additaments</strong><br />
        All and any kind of additament solicited after the order confirmation will just be 
        accepted if presented in written(by formal letter or email). If it’s approved, it 
        will be considered part of the buying process. Verbal solicitations won’t be processed.
    </p>
    <p>
        <strong>Penalty and Financial Obligation in result of Overdue Payments</strong><br />
        The value and date established on the invoice must be respected, on punishment of 
        paying 10% (ten percent) penalty and interests of 4% (four percent) a year.
    </p>
    <p>
        <strong>Unexpected Events (Environmental, Criminal and Others)</strong><br />
        Both customers and Coopersalto are considered free of contractual duties if: 
    </p>
    <ul>
        <li>Blackouts, burning or flood officially stated as catastrophic;</li>
        <li>Legal resolution that exceed the will of both customer and supplier and doesn’t 
            depend on Coopersalto or the customer.</li>
    </ul>
    <p>
        <strong>Material Guarantee</strong><br />
        Coopersalto accepts the responsibility for production fault for a period of 12 (twelve) 
        months counting from the material receiving and has the right to inspect any 
        defective material, before the retreat or devolution.
    </p>
    <p>
        All the material on the orders will be manufactured and there might be a variation of 
        about 5% (five percent) surplus or minus on the cable length, considering the 
        whole bobbin or the whole supplied cable.
    </p>
    
<?php } else { ?>
<!-- versão em português --> 

    <h2 class="title2">Termos de Venda</h2>
    <p class="title2">
        Termos e Condições Gerais para Fornecimento de Material
    </p>
    <p>
        <strong>Objeto</strong><br />
        As condições gerais de venda estabelecidas neste instrumento fazem parte da proposta comercial
        da Coopersalto que ora apresentamos a V.Sas. Na eventualidade de haver divergências nas 
        condições comerciais que não estejam previstas na proposta comercial, serão levadas em 
        consideração apenas as seguintes cláusulas:
    </p>
    <p>
        <strong>Validade da Proposta</strong><br />
        A proposta comercial acima mencionada será válida pelo período nela estabelecido, contado em 
        dias corridos a partir da data de apresentação da proposta e podendo, eventualmente, ser 
        revisada mediante acordo entre a parte contratante e a Coopersalto.
    </p>
    <p>
        <strong>Preços e Condições Especiais</strong><br />
        Os preços e condições comerciais estabelecidos são aqueles indicados em nossa proposta 
        comercial acima mencionada e serão reajustados em cada processo de faturamento, de acordo com 
        as fórmulas estabelecidas em nossa proposta comercial e mutuamente acordada.
    </p>
    <p>
        <strong>Aprovação de Créditos</strong><br />
        O fornecimento dos materiais relacionados em nossa proposta está condicionado as informações 
        e garantias dadas pela parte Contratante, para fins de aprovação de crédito referente ao valor 
        total da compra.
    </p>
    <p>
        <strong>Tributos</strong><br />
        Os tributos considerados na proposta acima mencionada estão de acordo com as faixas de 
        tributação em vigor na presente data. Quaisquer modificações nessas faixas ou base de cálculo 
        dos impostos e taxas, ou a criação de novos impostos e/ou alíquotas de impostos que venham a 
        incidir sobre o presente fornecimento, resultarão na correspondente modificação dos preços 
        propostos de forma que seja mantido o equilíbrio econômico inicial desta proposta.
    </p>
    <p>
        <strong>Aditamentos Contratuais</strong><br />
        Todos e quaisquer aditamentos contratuais que venham a ser solicitados após a confirmação do 
        pedido somente serão aceitos se formulados por escrito. Se os aditamentos puderem ser aceitos, 
        suas confirmações serão processadas mediante o envio de correspondência oficial (por exemplo, 
        carta, e-mail ou outro) entre as partes, que será considerada, a partir de agora, como parte 
        do procedimento de compra. Não serão processados adiantamentos pró-solicitações verbais.
    </p>
    <p>
        <strong>Penalidade e Obrigações Financeiras decorrentes de Pagamento em Atraso</strong><br />
        Os valores devidos deverão ser pagos nas datas estabelecidas na fatura, sob pena de estarem 
        sujeitos a uma multa de 10% (dez por cento) e juros de 4% (quatro por cento) ao ano.
    </p>
    <p>
        <strong>Eventos Imprevistos (Ambientais, Criminais e Outros)</strong><br />
        Serão considerados como “eventos imprevistos”, de forma que ambas as partes serão consideradas 
        liberadas do cumprimento de todas as obrigações contratuais, os seguintes fatos:
    </p>
    <ul>
        <li>
            Quedas de energia, incêndios ou inundações oficialmente declarados como catastróficos;
        </li>
        <li>
            Determinações legais que excedam a vontade das partes e que não dependam de decisões da 
            Coopersalto
        </li>
        <li>
            ou da parte contratante.
        </li>
    </ul>
    <p>
        <strong>Garantia do Material</strong><br />
        A Coopersalto garante a qualidade dos materiais contra defeitos de produção por um período 
        de 12 (doze) meses após a data de recebimento do material, e reserva-se o direito de 
        inspecionar todo e qualquer material defeituoso descrito neste fornecimento, que 
        eventualmente venha a ser detectado pela parte contratante, antes da retirada e/ou 
        devolução do referido material.
    </p>
    <p>
        Todos os materiais serão fabricados e deverá ser considerada permissível uma variação de 
        até +/- 5% (mais ou menos cinco por cento) no comprimento do cabo, considerado o comprimento 
        de cada bobina ou o comprimento total do cabo fornecido.
    </p>
    
<?php } ?>