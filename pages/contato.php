<?php if ($curLang == 'en') { ?>
<!-- versão em inglês -->  

    <h2 class="title2">Contact Us</h2>
    <p class="title2">
        Coopersalto offers you many different ways to get in touch with us
    </p>
    <p>
        Marechal Rondon Street nº 2170 - Pedregulho<br />
        Zip Code: 13323.505 - Salto - SP
    </p>
    <p style="width: 100px; float: left;">
        <strong>Phones:</strong><br />     
    </p>    
    <p style="width: 230px; float: left;">
        <strong>+55 11 4028.9696 (PABX)</strong><br />
        <strong>+55 11 4028.9691</strong><br />
    	<strong>+55 11 4028.9697</strong><br />
    	<strong>+55 11 4028-9690</strong><br />  
		<strong>+55 11 4028-9692</strong><br />    
		<strong>+55 11 4028-9694</strong><br /> 
		<strong>+55 11 4028-9698</strong><br /> 
		<strong>+55 11 4028-9699</strong><br /> 
    </p>
    <p style="width: 300px; float: left;">
        <a href="mailto:vendas@coopersalto.com.br">vendas@coopersalto.com.br</a><br />
        <a href="mailto:francisco@coopersalto.com.br">francisco@coopersalto.com.br</a><br />   
        <a href="mailto:marcelo@coopersalto.com.br">marcelo@coopersalto.com.br</a><br />   
        <a href="mailto:rita@coopersalto.com.br">rita@coopersalto.com.br</a><br />	 
		<a href="mailto:financeiro@coopersalto.com.br">financeiro@coopersalto.com.br</a><br />
		<a href="mailto:faturamento@coopersalto.com.br">faturamento@coopersalto.com.br</a><br />
		<a href="mailto:compras@coopersalto.com.br">compras@coopersalto.com.br</a><br />
		<a href="mailto:rh@coopersalto.com.br">rh@coopersalto.com.br</a></br>
    </p>
	<div style="float: left; margin: 30px 15px;">
		<iframe width="600" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Rua+Mal+Rondon,+2170+-+Salto,+13323-505&amp;sll=-23.217231,-47.264986&amp;sspn=0.016032,0.027874&amp;vpsrc=6&amp;ie=UTF8&amp;hq=&amp;hnear=R.+Mal.+Rondon,+2170+-+Salto+-+S%C3%A3o+Paulo,+13323-505,+Brazil&amp;t=m&amp;ll=-23.207351,-47.264128&amp;spn=0.02761,0.036478&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
		<br />
		<small>
			<a target="_blank" href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Rua+Mal+Rondon,+2170+-+Salto,+13323-505&amp;aq=&amp;sll=38.341656,-95.712891&amp;sspn=55.0521,114.169922&amp;vpsrc=0&amp;ie=UTF8&amp;hq=Rua+Mal+Rondon,+2170+-&amp;hnear=13323-505,+Brazil&amp;t=m&amp;z=16&amp;iwloc=A" style="text-align:center;display:block">View Larger Map</a>
		</small>
	</div>
	
<?php } else { ?>
<!-- versão em português --> 

    <h2 class="title2">Contato</h2>
    <p class="title2">
        A Coopersalto disponibiliza diversos meios para você tirar suas dúvidas
    </p>
    <p>
        Rua Marechal Rondon nº 2170 - Pedregulho<br />
        Cep: 13323.505 - Salto - SP
    </p>
    <p style="width: 100px; float: left;">
        <strong>Phones:</strong><br />     
    </p>    
    <p style="width: 230px; float: left;">
        <strong>+55 11 4028.9696 (PABX)</strong><br />
        <strong>+55 11 4028.9691</strong><br />
    	<strong>+55 11 4028.9697</strong><br />
    	<strong>+55 11 4028-9690</strong><br />
		<strong>+55 11 4028-9692</strong><br />    
		<strong>+55 11 4028-9694</strong><br /> 
		<strong>+55 11 4028-9698</strong><br /> 
		<strong>+55 11 4028-9699</strong><br /> 		
    </p>
    <p style="width: 300px; float: left;">
        <a href="mailto:vendas@coopersalto.com.br">vendas@coopersalto.com.br</a><br />
        <a href="mailto:francisco@coopersalto.com.br">francisco@coopersalto.com.br</a><br />  
        <a href="mailto:marcelo@coopersalto.com.br">marcelo@coopersalto.com.br</a><br />    
        <a href="mailto:rita@coopersalto.com.br">rita@coopersalto.com.br</a><br />	 
		<a href="mailto:financeiro@coopersalto.com.br">financeiro@coopersalto.com.br</a><br />
		<a href="mailto:faturamento@coopersalto.com.br">faturamento@coopersalto.com.br</a><br />
		<a href="mailto:compras@coopersalto.com.br">compras@coopersalto.com.br</a><br />
		<a href="mailto:rh@coopersalto.com.br">rh@coopersalto.com.br</a></br>
    </p>
	<div style="float: left; margin: 30px 15px;">
		<iframe width="600" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=+&amp;q=Rua+Mal+Rondon,+2170+-+Salto,+13323-505&amp;aq=&amp;ie=UTF8&amp;hq=&amp;hnear=R.+Mal.+Rondon,+2170+-+Salto+-+S%C3%A3o+Paulo,+13323-505&amp;t=m&amp;vpsrc=6&amp;ll=-23.205694,-47.262411&amp;spn=0.029977,0.053988&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
		<br />
		<small>
			<a target="_blank" href="http://maps.google.com.br/maps?f=q&amp;source=embed&amp;hl=pt-BR&amp;geocode=+&amp;q=Rua+Mal+Rondon,+2170+-+Salto,+13323-505&amp;aq=&amp;ie=UTF8&amp;hq=&amp;hnear=R.+Mal.+Rondon,+2170+-+Salto+-+S%C3%A3o+Paulo,+13323-505&amp;t=m&amp;vpsrc=6&amp;ll=-23.205694,-47.262411&amp;spn=0.029977,0.053988&amp;z=14&amp;iwloc=A" style="text-align:center;display:block">Exibir mapa ampliado</a>
		</small>
	</div>

<?php } ?>