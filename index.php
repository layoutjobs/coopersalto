<?php
// ** CAPTURA PÁGINA SOLICITADA **
if (isset($_GET['language'])) {
    $curLang = $_GET['language'];
} else {
    $curLang = 'pt';    
}
if (isset($_GET['page'])) { 
    $curPage = $_GET['page'];
} else {
    $curPage = 'home';    
}
if (isset($_GET['session'])) {
    $curSession = $_GET['session'];
} else {
    $curSession = $curPage;    
}

$indexDir = 'http://'. str_replace('www.', '', $_SERVER['HTTP_HOST']) . str_replace('/index.php', '', $_SERVER['SCRIPT_NAME']) . '/';
$basePath = $indexDir . $curLang . '/' ;            
$imagesPath = $indexDir . 'images/';
$cssPath = $indexDir . 'css/';
$jsPath = $indexDir . 'js/';
$mediaPath = $indexDir . 'media/';
$curURL = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

// ** CONFIGURAÇÃO DAS PÁGINAS **
$pageNameEn = '';

if ( $curPage == 'home') {
  $pageName = 'Coopersalto | Bem-vindo';
  $pageNameEn = 'Coopersalto | Welcome';
  $pageKeywords = ''; 
  $pageDescription = '';
  $pageFile = 'home.php';
  $sidebarFile = '';
} else if ( $curPage == 'a-coopersalto') {
  $pageName = 'Coopersalto | A Coopersalto'; 
  $pageNameEn = 'Coopersalto | Coopersalto';
  $pageKeywords = ''; 
  $pageDescription = '';
  $pageFile = 'a-coopersalto/quem-somos.php';
  $sidebarFile = 'sidebar.php';
    } else if ( $curPage == 'quem-somos') {
      $pageName = 'Coopersalto | Quem Somos'; 
  	  $pageNameEn = 'Coopersalto | About Us';
      $pageKeywords = ''; 
      $pageDescription = '';
      $pageFile = 'a-coopersalto/quem-somos.php';
      $sidebarFile = 'sidebar.php';
    } else if ( $curPage == 'politica-de-qualidade') {
      $pageName = 'Coopersalto | Política de Qualidade'; 
	  $pageNameEn = 'Coopersalto | Quality Policy';
      $pageKeywords = ''; 
      $pageDescription = '';
      $pageFile = 'a-coopersalto/politica-de-qualidade.php';
      $sidebarFile = 'sidebar.php';
    } else if ( $curPage == 'politica-ambiental') {
      $pageName = 'Coopersalto | Política Ambiental'; 
	  $pageNameEn = 'Coopersalto | Environmental Policy';
      $pageKeywords = ''; 
      $pageDescription = '';
      $pageFile = 'a-coopersalto/politica-ambiental.php';
      $sidebarFile = 'sidebar.php';
    } else if ( $curPage == 'iso-9001-2015') {
      $pageName = 'Coopersalto | ISO 9001:2015'; 
	  $pageNameEn = 'Coopersalto | ISO 9001:2015';
      $pageKeywords = ''; 
      $pageDescription = '';
      $pageFile = 'a-coopersalto/iso-9001-2015.php';
      $sidebarFile = 'sidebar.php';
    } else if ( $curPage == 'termos-de-venda') {
      $pageName = 'Coopersalto | Termos de Venda'; 
	  $pageNameEn = 'Coopersalto | Terms of Sale';
      $pageKeywords = ''; 
      $pageDescription = '';
      $pageFile = 'a-coopersalto/termos-de-venda.php';
      $sidebarFile = 'sidebar.php';
    } else if ( $curPage == 'parceiros') {
      $pageName = 'Coopersalto | Parceiros'; 
	  $pageNameEn = 'Coopersalto | Partners';
      $pageKeywords = ''; 
      $pageDescription = '';
      $pageFile = 'a-coopersalto/parceiros.php';
      $sidebarFile = 'sidebar.php';


    } else if ( $curPage == 'aumente-a-satisfacao-do-seu-cliente') {
      $pageName = 'Coopersalto | Aumente a Satisfação do seu Cliente'; 
    //$pageNameEn = 'Coopersalto | Terms of Sale';
      $pageKeywords = ''; 
      $pageDescription = '';
      $pageFile = 'a-coopersalto/aumente-a-satisfacao-do-seu-cliente.php';
      $sidebarFile = 'sidebar.php';
    } else if ( $curPage == 'sem-mais-desculpas-para-nao-botar-pra-fazer') {
      $pageName = 'Coopersalto | Sem Mais Desculpas Para Não Botar Para Fazer'; 
    //$pageNameEn = 'Coopersalto | Terms of Sale';
      $pageKeywords = ''; 
      $pageDescription = '';
      $pageFile = 'a-coopersalto/sem-mais-desculpas-para-nao-botar-pra-fazer.php';
      $sidebarFile = 'sidebar.php';

    } else if ( $curPage == 'o-que-e-tecnologia-da-informacao') {
      $pageName = 'Coopersalto | O que é Tecnologia da Informação'; 
    //$pageNameEn = 'Coopersalto | Terms of Sale';
      $pageKeywords = ''; 
      $pageDescription = '';
      $pageFile = 'a-coopersalto/o-que-e-tecnologia-da-informacao.php';
      $sidebarFile = 'sidebar.php';  

} else if ( $curPage == 'produtos') {
  $pageName = 'Coopersalto | Produtos';
  $pageNameEn = 'Coopersalto | Products';
  $pageKeywords = ''; 
  $pageDescription = '';
  $pageFile = 'produtos/cce-apl.php';
  $sidebarFile = 'sidebar.php';
    } else if ( $curPage == 'familia-cce-apl') {
      $pageName = 'Coopersalto | Família CCE-APL';
      $pageKeywords = ''; 
      $pageDescription = '';
      $pageFile = 'produtos/cce-apl.php';
      $sidebarFile = 'sidebar.php';
    } else if ( $curPage == 'familia-cce-apl-asf') {
      $pageName = 'Coopersalto | Família CCE-APL-ASF';
      $pageKeywords = ''; 
      $pageDescription = '';
      $pageFile = 'produtos/cce-apl-asf.php';
      $sidebarFile = 'sidebar.php';
    } else if ( $curPage == 'familia-cce-apl-g') {
      $pageName = 'Coopersalto | Família CCE-APL-G';
      $pageKeywords = ''; 
      $pageDescription = '';
      $pageFile = 'produtos/cce-apl-g.php';
      $sidebarFile = 'sidebar.php';
    } else if ( $curPage == 'familia-cci') {
      $pageName = 'Coopersalto | Família CCI';
      $pageKeywords = ''; 
      $pageDescription = '';
      $pageFile = 'produtos/cci.php';
      $sidebarFile = 'sidebar.php';
    } else if ( $curPage == 'familia-cti-pe') {
      $pageName = 'Coopersalto | Família CTI-PE';
      $pageKeywords = ''; 
      $pageDescription = '';
      $pageFile = 'produtos/cti-pe.php';
      $sidebarFile = 'sidebar.php';
    } else if ( $curPage == 'familia-ctp-apl') {
      $pageName = 'Coopersalto | Família CTP-APL';
      $pageKeywords = ''; 
      $pageDescription = '';
      $pageFile = 'produtos/ctp-apl.php';
      $sidebarFile = 'sidebar.php';
    } else if ( $curPage == 'familia-ctp-apl-g') {
      $pageName = 'Coopersalto | Família CTP-APL-G';
      $pageKeywords = ''; 
      $pageDescription = '';
      $pageFile = 'produtos/ctp-apl-g.php';
      $sidebarFile = 'sidebar.php';
    } else if ( $curPage == 'familia-ctp-apl-sn') {
      $pageName = 'Coopersalto | Família CTP-APL-SN';
      $pageKeywords = ''; 
      $pageDescription = '';
      $pageFile = 'produtos/ctp-apl-sn.php';
      $sidebarFile = 'sidebar.php';
    } else if ( $curPage == 'familia-fdg') {
      $pageName = 'Coopersalto | Família FDG';
      $pageKeywords = ''; 
      $pageDescription = '';
      $pageFile = 'produtos/fdg.php';
      $sidebarFile = 'sidebar.php';
    } else if ( $curPage == 'familia-fi') {
      $pageName = 'Coopersalto | Família FI';
      $pageKeywords = ''; 
      $pageDescription = '';
      $pageFile = 'produtos/fi.php';
      $sidebarFile = 'sidebar.php';
    } else if ( $curPage == 'cabo-lan') {
      $pageName = 'Coopersalto | Cabo LAN';
      $pageKeywords = ''; 
      $pageDescription = '';
      $pageFile = 'produtos/cabo-lan.php';
      $sidebarFile = 'sidebar.php';
} else if ( $curPage == 'contato') {
  $pageName = 'Coopersalto | Contato';
  $pageNameEn = 'Coopersalto | Contact Us';
  $pageKeywords = ''; 
  $pageDescription = '';
  $pageFile = 'contato.php';
  $sidebarFile = 'sidebar.php';
} else {
  $pageName = 'Coopersalto | Erro 404';
  $pageNameEn = 'Coopersalto | 404 Error';
  $pageKeywords = ''; 
  $pageDescription = '';
  $pageFile = 'error404';
  $sidebarFile = 'sidebar.php';
}

if ($pageNameEn == '') $pageNameEn = $pageName;
if ($curLang == 'en') $pageName = $pageNameEn;

// ** CARREGA O HEADER **
include('includes/header.php');

// ** CARREGA O SIDEBAR **
if ($sidebarFile <> '') include('includes/' . $sidebarFile);
?>
   
<div id='article'>

<?php
// ** CARREGA A PÁGINA SOLICITADA OU ERRO **
if ($pageFile == "error404") {
    echo ('<p class="error">Desculpe, página não encontrada.</p>');  	
} else {
    include('pages/' . $pageFile); 
}
?>

</div><!-- end #article -->

<?php 
// ** CARREGA O FOOTER **
include('includes/footer.php'); 
?>

